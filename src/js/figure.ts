abstract class Figure{
    constructor(public _name:string, protected _type:string, protected _x:number, protected _y:number){}

    get name():string{
        return this._name;
    }

    get type():string{
        return this._type;
    }

    get x():number{
        return this._x;
    }

    get y():number{
        return this._y
    }

}

export class Circle extends Figure{

    constructor(_name:string, _type:string, _x:number, _y:number, protected _radius:number, protected _perimeter:number, protected _area:number){
        super(_name, _type, _x, _y);
    }

    get radius():number{
        return this._radius;
    }

    get perimeter():number{
        return this._perimeter;
    }

    get area():number{
        return this._area;
    }
}

export class Triangle extends Figure{
    constructor(_name:string, _type:string, _x:number, _y:number, protected _width:number, protected _perimeter:number, protected _area:number){
        super(_name, _type, _x, _y);
    }

    get width():number{
        return this._width;
    }

    get perimeter():number{
        return this._perimeter;
    }

    get area():number{
        return this._area;
    }
}

export class Rectangle extends Figure{

    constructor(_name:string, _type:string, _x:number, _y:number, protected _width:number, protected _height:number, protected _perimeter:number, protected _area:number){
        super(_name, _type, _x, _y);
    }

    get width():number{
        return this._width;
    }

    get height():number{
        return this._height;
    }

    get perimeter():number{
        return this._perimeter;
    }

    get area():number{
        return this._area;
    }

}

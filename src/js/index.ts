declare function require(string): any;
const css = require('./style.sass');
import $ from 'jquery-ts';
import {Circle, Triangle, Rectangle} from './figure'

enum figureType {
    circle = 1,
    triangle = 2,
    rectangle = 3
}

let dataToRender = [];

window.onload = function(){
    dataToRender = JSON.parse(localStorage['data']);
    if(dataToRender.length>0){
        render(dataToRender);
    }
};

function render(data){
    if(Array.isArray(data)){
        data.forEach((elem, i)=>{
            $('#mainTable > tbody:last-child')
                .append(
                    `<tr>
                    <td>${elem._name}</td>
                    <td>${elem._type}</td>
                    <td>${elem._x}</td>
                    <td>${elem._y}</td>
                    <td>${elem._width || "-"}</td>
                    <td>${elem._height || "-"}</td>
                    <td>${elem._radius || "-"}</td>
                    <td>${(elem._perimeter*1).toFixed(1)}</td>
                    <td>${(elem._area*1).toFixed(1)}</td>
                    <td><button type="button" class="btn btn-danger delete" deleteId=${i}>Delete</button></td>
                </tr>`)
        })
    }
    else{
        console.log('wrong data');
    }
}

let pi:number = 3.14;

$('#addbtn').on('click',()=>{
    let type = +$('#type').val();
    switch(type){
        case figureType.circle : {
            let rad:number = +$('#radius').val();
            let data = {
                name: $('#name').val(),
                type: type,
                x: +$('#x').val(),
                y: +$('#y').val(),
                radius: rad,
                perimeter: rad * pi * 2,
                area: rad*rad*pi
            };
            let figure = new Circle(data.name, figureType[data.type], data.x, data.y, data.radius, data.perimeter, data.area);
            dataToRender.push(figure);
            break;
        }
        case figureType.triangle : {
            let wid:number = +$('#width').val();
            let data = {
                name: $('#name').val(),
                type: type,
                x: +$('#x').val(),
                y: +$('#y').val(),
                width: wid,
                perimeter: wid * 3,
                area: (Math.sqrt(3) / 2) * wid * wid
            };
            let figure = new Triangle(data.name, figureType[data.type], data.x, data.y, data.width, data.perimeter, data.area);
            dataToRender.push(figure);
            break;
        }
        case figureType.rectangle : {
            let wid:number = +$('#width').val();
            let hei:number = +$('#height').val();
            let data = {
                name: $('#name').val(),
                type: type,
                x: +$('#x').val(),
                y: +$('#y').val(),
                width: wid,
                height: hei,
                perimeter: wid*2 + hei*2,
                area: hei * wid
            };
            let figure = new Rectangle(data.name, figureType[data.type], data.x, data.y, data.width, data.height, data.perimeter, data.area);
            dataToRender.push(figure);
            break;
        }
    }
    localStorage.setItem('data', JSON.stringify(dataToRender));
    clearTable();
    render(dataToRender);
});

$(document).ready(function() {

    $('#type').change(function(){
        let choice = $('#type :selected').val();
        switch(choice) {
            case "1" :
                $('#width').hide();
                $('#height').hide();
                $('#radius').show();
                break;
            case "2":
                $('#width').show();
                $('#height').hide();
                $('#radius').hide();
                break;
            case "3":
                $('#width').show();
                $('#height').show();
                $('#radius').hide();
                break;
            default:
                $('#width').hide();
                $('#height').hide();
                $('#radius').hide();
                break;
        }

});
});

$(document).on("click", ".delete", function () {
    let id = +($(event.target).attr('deleteId'));
    dataToRender.splice(id, 1);
    localStorage.setItem('data', JSON.stringify(dataToRender));
    clearTable();
    render(dataToRender);
});

function clearTable(){
    $('#mainTable > tbody > tr').remove();
}